<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class GameFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'game' => substr(sha1(time()), 0, 10),
            'player_id' => 1,
            'box_1' => $this->faker->randomElement([0, 1, null]),
            'box_2' => $this->faker->randomElement([0, 1, null]),
            'box_3' => $this->faker->randomElement([0, 1, null]),
            'box_4' => $this->faker->randomElement([0, 1, null]),
            'box_5' => $this->faker->randomElement([0, 1, null]),
            'box_6' => $this->faker->randomElement([0, 1, null]),
            'box_7' => $this->faker->randomElement([0, 1, null]),
            'box_8' => $this->faker->randomElement([0, 1, null]),
            'box_9' => $this->faker->randomElement([0, 1, null]),
            'turn' => $this->faker->randomElement([0, 1]),
            'winner' => null,
        ];
    }
}
