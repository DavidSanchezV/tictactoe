<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->string('game')->comment('Id de la partida.');
            $table->unsignedInteger('player_id')->comment('Id del jugador.');
            $table->binary('box_1')->nullable()->comment('Casilla 1');
            $table->binary('box_2')->nullable()->comment('Casilla 2');
            $table->binary('box_3')->nullable()->comment('Casilla 3');
            $table->binary('box_4')->nullable()->comment('Casilla 4');
            $table->binary('box_5')->nullable()->comment('Casilla 5');
            $table->binary('box_6')->nullable()->comment('Casilla 6');
            $table->binary('box_7')->nullable()->comment('Casilla 7');
            $table->binary('box_8')->nullable()->comment('Casilla 8');
            $table->binary('box_9')->nullable()->comment('Casilla 9');
            $table->binary('winner')->nullable()->comment('1: gana el jugador creador de la partida, 0: gana el invitado'
                . ', null: Aún no hay ganador.');
            $table->binary('turn')->comment('Tuno en la que va la partida');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
