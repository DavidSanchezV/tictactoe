<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $fillable = [
        'game',
        'player_id',
        'turn'
    ];

    /**
     * Retorna el jugador
     *
     * @return App\Models\Player;
     */
    public function player()
    {
        return $this->belongsTo(Player::class);
    }

    /**
     * Filtra el juego por el id de la partida
     *
     * @param $query
     * @param integer $game
     * @return App\Models\Game;
     */
    public function scopeGame($query, $game)
    {
        if ($game) {
            return $query->where('game', $game);
        }
    }
}
