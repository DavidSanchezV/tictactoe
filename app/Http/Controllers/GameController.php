<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GameController extends Controller
{
    /**
     * Crea la partida
     *
     * @param Request $request
     * @return json
     */
    public function store(Request $request)
    {
        $request->request->add(['game' => substr(sha1(time()), 0, 10)]);
        $request->request->add(['turn' => true]);
        $game = Game::create($request->all());

        return response()->json($game);
    }

    /**
     * Actualiza la partida
     *
     * @param Game $game
     * @param Request $request
     * @return json
     */
    public function update(Game $game, Request $request)
    {
        $box = $request->box;
        $game->$box = $request->value;
        $game->turn = $request->value;
        $saved = $game->save();
        if ($saved) {
            $winner = $this->searchWinner(Game::find($game->id));
        }

        return response()->json(['winner' => $winner]);
    }

    /**
     * Busca la partia
     *
     * @param Request $request
     * @return json
     */
    public function edit(Request $request)
    {
        $hasError = false;
        $game = null;

        $validator = Validator::make($request->all(), [
            'game' => 'required|string|size:10'
        ]);

        if ($validator->fails()) {
            $hasError = true;
        } else {
            $game = Game::game($request->game)->first();
            $game->playerName = $game->player->player;
            if (is_null($game)) {
                $hasError = true;
            } else {
                if ($game->winner === null) {
                    $game->winner = $this->searchWinner($game);
                }
            }
        }

        return response()->json(['hasError' => $hasError, 'game' => $game]);
    }

    /**
     * Busca el ganador
     *
     * @param Game $game
     * @return boolean
     */
    private function searchWinner(Game $game)
    {
        $winner = null;
        $winningConditions = [
            ["box_1", 'box_2', 'box_3'],
            ['box_4', 'box_5', 'box_6'],
            ['box_7', 'box_8', 'box_9'],
            ['box_1', 'box_4', 'box_7'],
            ['box_2', 'box_5', 'box_8'],
            ['box_3', 'box_6', 'box_9'],
            ['box_1', 'box_5', 'box_9'],
            ['box_3', 'box_5', 'box_7']
        ];
        
        for ($i=0; $i < 8; $i++) { 
            $winCondition = $winningConditions[$i];
            $value1 = $game->{$winCondition[0]};
            $value2 = $game->{$winCondition[1]};
            $value3 = $game->{$winCondition[2]};

            if ($value1 === null || $value2 === null || $value3 === null) {
                continue;
            }
            if ($value1 === $value2 && $value2 === $value3) {
                $winner = boolval($value1);
                break;
            }
        }

        return $winner;
    }
}
