<?php

namespace App\Http\Controllers;

use App\Models\Player;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    /**
     * Crea el jugador
     *
     * @param Request $request
     * @return json
     */
    public function store(Request $request)
    {
        $player = Player::create($request->all());

        return response()->json($player);
    }
}
