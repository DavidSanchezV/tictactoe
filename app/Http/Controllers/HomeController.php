<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Retorna la vista principal
     *
     * @return view
     */
    public function index()
    {
        return view('home');
    }
}
