## Como probar la aplicación

Precondiciones:

1. Tener instalado PHP 7.3 o superior.
2. Tener instalado Composer.
3. Tener instalado NodeJS y NPM.

Procedimieno:

1. Clonar el respositorio.
2. Ejecutar el comando: composer install.
3. Ejecutar el comando: npm install.
4. Ejecutar el comando: npm run prod.

Nota: se puede usar el comando php artisan serve, para correr la aplicacion en un entorno local.

Con lo anterior ya se puede hacer uso de la aplicacion.
